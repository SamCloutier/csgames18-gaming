from django.contrib.admin import register
from django.contrib import admin
from django.db import models


class SiteParams(models.Model):

  class Meta:
    verbose_name_plural = "Site-Wide Parameters"
    app_label = 'cs18_gaming'

  def __str__(self):
    return "Parameters"

  # Makes the site publicly visible and enables the score registering
  site_enabled = models.BooleanField(default=True)

  # Makes the scoreboard accessible
  scoreboard_enabled = models.BooleanField(default=True)

  # max false score submit attemps before a team is banned from playing
  max_false_score_attempts = models.IntegerField(default=20)

  @staticmethod
  def get(paramName):
    if SiteParams.objects.count() == 0:
      raise Exception("Site-wide params have not been innitialised. Run 'manage.py initData'")
    if SiteParams.objects.count() > 1:
      raise Exception("No more than one instance of <SiteParams> should ever exist!")
    return getattr(SiteParams.objects.get(pk=1), paramName)


@register(SiteParams)
class SiteParamsModelAdmin(admin.ModelAdmin):
  pass
