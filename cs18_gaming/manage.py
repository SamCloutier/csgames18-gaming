#!/usr/bin/env python
import os
import sys

try:
  from cs18_gaming import settings
except ImportError as exc:
  raise ImportError(
    "Couln't find the settings file. Did you forget to copy "
    "cs18_gaming/clean-setting.py into cs18_gaming/settings.py?"
  ) from exc

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "cs18_gaming.settings")
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)
