from django.http import Http404

from cs18_gaming.models import SiteParams


def settingsVariablesProcessor(request):
  site_enabled = SiteParams.get('site_enabled')
  scoreboard_anabled = SiteParams.get('scoreboard_enabled')
  max_false_score_attempts = SiteParams.get('max_false_score_attempts')

  if not site_enabled \
      and not request.user.is_authenticated\
      and request.META['PATH_INFO'] not in ['/admin/login/', '/scoreboard', ]:
    raise Http404()

  if not scoreboard_anabled \
      and not request.user.is_authenticated \
      and request.META['PATH_INFO'] == '/scoreboard':
    raise Http404()

  return {
    'MAX_FALSE_SCORE_ATTEMPS': max_false_score_attempts,
    'BAN_WARNING_THRESHOLD': int(0.8*max_false_score_attempts),
    'SITE_ENABLED':site_enabled,
    'SCOREBOARD_ENABLED': scoreboard_anabled,
  }

