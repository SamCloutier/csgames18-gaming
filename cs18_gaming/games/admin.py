from django.contrib import admin
from django.contrib.admin import register

from games.models import Team, Game, Score

# Register your models here.

@register(Team)
class TeamModelAdmin(admin.ModelAdmin):
  list_display = ('name', 'universityName', 'falseScoreCount')
  ordering = ['-falseScoreCount']

@register(Game)
class GameModelAdmin(admin.ModelAdmin):
  list_display = ('gameID', 'name', 'description')

@register(Score)
class ScoreModelAdmin(admin.ModelAdmin):
  list_display = ('team', 'game', 'score')

