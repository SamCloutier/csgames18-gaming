from functools import reduce

from django.db import models

from cs18_gaming.models import SiteParams


class Team(models.Model):
  name = models.CharField(max_length=512)
  universityName = models.CharField(max_length=512)
  falseScoreCount = models.IntegerField(default=0)

  def __str__(self):
    return 'Team %s' % (self.name)

  def getTotalScore(self):
    if not self.getRankedScores(): return 0
    return reduce((lambda a, b: a + b), list(map(lambda s:s.score, self.getRankedScores())))

  def getRankedScores(self):
    """
    A score ranks only if it is within the 10 best scores
    :return: a sorted array of the (up to) ten best scores for the given game.
    """
    rankedScores = []
    for game in Game.objects.all():
      bestRegisteredScores = game.getBestScores(10)
      bestTeamScore = self.scores.filter(game=game).order_by('-score').first()
      if bestTeamScore in bestRegisteredScores:
        rankedScores.append(bestTeamScore)
    return sorted(rankedScores, key=lambda rankedGame: rankedGame.score, reverse=True)

  def update(self, jObject):
    self.name = jObject['name']
    self.universityName = jObject['universityName']
    self.save()


class Game(models.Model):
  name = models.CharField(max_length=512)
  gameID = models.IntegerField()
  description = models.CharField(max_length=2048)
  available = models.BooleanField(default=True)

  def __str__(self):
    return 'Game %s - %s' % (self.gameID, self.name)

  def getCurrentHighScore(self):
    return self.getBestScores(1).first()

  def getBestScores(self, count=10):
    max_false_score_attempts = SiteParams.get('max_false_score_attempts')
    return self.scores.filter(team__falseScoreCount__lt=max_false_score_attempts).order_by('-score')[:count]

  def update(self, jObject):
    self.name = jObject['name']
    self.description = jObject['description']
    self.available = jObject['available']
    self.save()


class Score(models.Model):
  score = models.FloatField()
  team = models.ForeignKey(Team, related_name="scores", on_delete=models.CASCADE)
  game = models.ForeignKey(Game, related_name="scores", on_delete=models.CASCADE)

  def __str__(self):
    return "%s score on %s: %s" % (self.team, self.game, self.score)

  def getRank(self):
    return list(self.game.getBestScores()).index(self)+1

