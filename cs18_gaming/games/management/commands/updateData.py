import os
from django.core.management.base import BaseCommand
import json

from cs18_gaming.models import SiteParams
from cs18_gaming.settings import DATA_DIRECTORY
from games.models import Team, Game


class Command(BaseCommand):
  help = "Initialize data for the CSGames launch"

  def handle(self, *args, **options):
    self.initSiteParams()
    self.updateTeams()
    self.updateGames()
    print('Data update completed')

  @staticmethod
  def initSiteParams():
    if not SiteParams.objects.exists():
      print("Generating site-wide params... ", end='', flush=True)
      SiteParams.objects.create()
      print('done')

  @staticmethod
  def updateTeams():
    print("Updating Teams... ", end='', flush=True)
    with open(os.path.join(DATA_DIRECTORY, 'teams.json'), encoding='utf8') as jTeams:
      for jObject in json.load(jTeams, encoding='utf8'):
        team, new = Team.objects.get_or_create(pk=jObject['id'])
        team.update(jObject)
    print('done')

  @staticmethod
  def updateGames():
    print("Updating Games... ", end='', flush=True)
    with open(os.path.join(DATA_DIRECTORY, 'games.json'), encoding='utf8') as jGames:
      for jObject in json.load(jGames, encoding='utf8'):
        game, new = Game.objects.get_or_create(gameID=jObject['gameID'])
        game.update(jObject)
    print('done')
