from django.urls import path
from games.views import *

urlpatterns = [
	path(r'', home),
	path(r'registerscore', registerScore),
	path(r'scoreboard', scoreBoard),
]

