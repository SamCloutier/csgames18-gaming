from django.db.models import Count, Sum
from django.http import Http404
from django.shortcuts import HttpResponse, render

from cs18_gaming.models import SiteParams
from cs18_gaming.settings import DEBUG
from games.models import Team, Game, Score


def closeTabResponse():
  return HttpResponse(
    '<script>window.onload = function (){window.close()}</script>Thank you! You can close this page now.')


def home(request):
  currentTeam = None
  currentGame = None

  if 'teamID' in request.GET and request.GET['teamID']:
    try:
      currentTeam = Team.objects.filter(pk=request.GET['teamID']).first()
    except:
      pass

  if 'gameID' in request.GET and request.GET['gameID']:
    try:
      currentGame = Game.objects.filter(gameID=request.GET['gameID']).first()
    except:
      pass

  return render(request, 'game.html', {
    'currentTeam': currentTeam,
    'currentGame': currentGame,
    'games': Game.objects.filter(available=True),
    'teams': Team.objects.order_by('universityName'),
  })


def registerScore(request):
  if not request.user.is_authenticated and not SiteParams.get("site_enabled"):
    raise Http404()
  try:
    score = float(request.GET['score'])
    teamID = int(request.GET['teamID'])
    gameID = int(request.GET['gameID'])
    receivedHash = int(request.GET['id'])

    calculatedHash = (int(((975632 + (221 * score - 10268)) % 262144) - 4203) + (teamID * 1245) - (
      gameID * 1245)) % 262144

    game = Game.objects.filter(gameID=gameID).first()
    team = Team.objects.filter(pk=teamID).first()
    if not game or not team:
      return closeTabResponse()

    if calculatedHash != receivedHash:
      team.falseScoreCount += 1
      team.save()
      return closeTabResponse()

    Score.objects.create(
      team=team,
      game=game,
      score=score
    )
  except:
    if DEBUG: raise

  print('score register sucess')
  return closeTabResponse()


def scoreBoard(request):
  reloadDelay = 0
  if 'reload' in request.GET:
    try:
      reloadDelay = max([int(request.GET['reload']), 3000])
    except:
      reloadDelay = 0

  limit = 30
  if 'limit' in request.GET and int(request.GET['limit']) > 0:
    limit = int(request.GET['limit'])

  if 'ranking' in request.GET:
    if request.GET['ranking'] == 'teams':
      return render(request, 'teamRankings.html',{
        'teams':[team for team in sorted(Team.objects.all(),
                                         key= lambda team : team.getTotalScore(), reverse=True)][:limit],
        'reloadDelay': reloadDelay,
      })
    elif request.GET['ranking'] == 'games':
      return render(request, 'gameRankings.html',{
        'games': Game.objects.annotate(scoreCount=Count('scores')).filter(scoreCount__gt=0)[:limit],
        'reloadDelay': reloadDelay,
      })

  return render(request, 'scoreboard.html',)
