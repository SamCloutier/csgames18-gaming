## CS GAMES 2018 Gaming Competition

#### Objective:
After selecting your team from the list as well as a game, try to register the best score you can. Since only the best score for a given game is used in calculating a given team score, you must perform equally well is as much of the available games as possible.  

#### Recommendation:
After some experimentation, it appears it is better to disable any [Flash Cheat Engine](https://www.raymond.cc/blog/download/did/1660/) while playing, since it can potentially harm the fun of playing and render the scoreboard obsolete.

---

#### Installation Setup: 

Assuming python \^3.5 is used as well as pip, the following steps will install the development site locally.

- Create a virtual environment and activate it:
    - `pip install virtualenv`
    - `virtualenv pyenv --python <absolute_path_to_python_35_executable>`
    - On Unix: `source pyenv/bin/activate`\
      On windows: `call pyenv/Scripts/activate.bat`
- Clone the project and install it:
    - `git clone https://gitlab.com/SamCloutier/csgames18-gaming csgaming`
    - `cd csgaming`
    - `pip install -r requirements.txt`
    - `cd cs18_gaming/cs18_gaming`
    - On Unix: `cp "settings-clean.py" "settings.py"`, \
      On Windows: `copy "settings-clean.py" settings.py`
    - `cd ..`
    - `python manage.py makemigrations cs18_gaming games`
    - `python manage.py migrate`
    - `python manage.py createsuperuser` (follow the instructions on screen)
- At this point feel free to edit the file `cs18_gaming/games/data/teams.json` if you wish to change the available players/teams
- Populating the database:
    - `python manage.py updateData`
- Running the server:
    - `python manage.py runserver`
    
---

#### Managing the site while playing:
Like any other Django site, you can visit [/admin](http://localhost:8000/admin) to edit, add or remove scores, as well as changing site settings to control user access. Use the superuser you created to log in as administrator.